import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Scanner;
public class Family {

    public static void main(String[] args) {

        String[][] schedule = new String[7][3];
        schedule[0][0] ="Mondey-Jumping";
        schedule[1][1] ="Mondey-Swimming";
        schedule[2][2] ="Mondey-Running";
        schedule[3][0] ="Mondey-Jumping";
        schedule[4][1] ="Mondey-Swimming";
        schedule[5][2] ="Mondey-Running";
        schedule[6][0] ="Mondey-Jumping";

        String[] habits = new String[3];
        habits[0] = "Eat";
        habits[1] = "Drink";
        habits[2] = "Sleep";
        Pet pet1 = new Pet("Dog","Layka");
        Pet pet2 = new Pet("Dog","Rock",5,75,habits);
        Pet pet3 = new Pet();

        Human human = new Human();
        Pet human1 = new Human("Jane","Karleone",1977);
        Human human1_0 = new Human("Jane","Karleone");
        Human human1_1 = new Human("Vito","Karleone");
        Human human2 = new Human("Michael","Karleone",1977,human1_0,human1_1);
        Human human3 = new Human( "Dog",  "Rock",  5,  75, habits, "Michael",  "Karleone",  1977,  90,  pet2,  human1_0,  human1_1,  "Sunday",  "Jumping", schedule);
        human.setNickName("Rock");
        human.setSpecies("Dog");
        human.setAge(5);

        System.out.println(human3.toString());
        System.out.println(pet2.toString());
        System.out.println(human.greetPet());
        System.out.println(human.describePet());
        System.out.println(pet3.eat());
        System.out.println(pet3.respond());
        System.out.println(pet3.foul());

    }

}

