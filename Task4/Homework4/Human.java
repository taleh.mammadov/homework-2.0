import java.util.Arrays;

public class Human extends Pet{
    private String name;
    private String surname;
    private int dateOfBirth;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String dayOfTheWeek;
    private String typeOfActivity;
    private String[][] schedule = new String[7][3];

    public Human(){
    }
    public Human(String[] habits){
        super(habits);
    }

    public Human(String name,String surname){
        this.name =name;
        this.surname  =surname;
    }

    public Human(String name,String surname,int dateOfBirth){
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }
    public Human(String name, String surname, int dateOfBirth, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.mother = mother;
        this.father = father;
    }
    public Human(String species, String nickName, int age, int trickLevel,String[] habits, String name, String surname, int dateOfBirth, int iq, Pet pet, Human mother, Human father, String dayOfTheWeek, String typeOfActivity, String[][] schedule) {
        super(species, nickName, age, trickLevel, habits);
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.dayOfTheWeek = dayOfTheWeek;
        this.typeOfActivity = typeOfActivity;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(String dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }

    public String getTypeOfActivity() {
        return typeOfActivity;
    }

    public void setTypeOfActivity(String typeOfActivity) {
        this.typeOfActivity = typeOfActivity;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    public String greetPet(){
        return "Hello, "+getNickName()+ ".";
    }
    public String describePet(){
        if(iq<=70){
            return "I have a "+getSpecies()+ ", he is "+getAge()+" years old, he is almost not sly";
        }else{
            return "I have a "+getSpecies()+ ", he is "+getAge()+" years old, he is very sly";
        }
    }


    @Override
    public String toString() {
        return "Human{" + "name='" + name + '\'' + ", surname='"
                + surname + '\'' + ", dateOfBirth=" + dateOfBirth + "," +
                " iq=" + iq +  ",pet="+pet+",}'}'}";
    }
}
