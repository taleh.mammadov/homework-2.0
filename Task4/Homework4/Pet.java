import java.lang.reflect.Array;
import java.util.Arrays;

public class Pet {
    private String species;
    private String nickName;
    private int age;
    private int trickLevel;
    protected String[] habits;


    public Pet(){

    }
    public Pet(String species, String nickName) {
        this.species = species;
        this.nickName = nickName;
    }

    public Pet(String species, String nickName, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String[] habits) {

    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String eat() {
        return "I am eating";
    }

    public String respond() {
        return "Hello, owner. I am - " + nickName + ". I miss you!";
    }

    public String foul() {
        return "I need to cover it up";
    }

    @Override
    public String toString() {
        return "Dog{" + "nickName='" + nickName + '\'' + ", age=" + age + ", trickLevel=" + trickLevel + ",habits= " + Arrays.toString(habits) + "}";
    }
}